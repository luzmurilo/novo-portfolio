import React from "react"

import "../styles/layout.css"

import { Header } from "./header"
import { Footer } from "./footer"

export function Layout({children}) {
    return <div className="layout-wrapper">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"></link>
        <div className="layout-header">
            <Header />
        </div>
        <div className="layout-body">
            <main className="layout-content">
                {children}
            </main>
        </div>
        <Footer />
    </div>
}