import React from "react"

import "../styles/card.css"

export function ProjectCard({children}) {
    return <div className="card">
        {children}
    </div>
}