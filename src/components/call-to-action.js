import React from "react"

import "../styles/call-to-action.css"
import UnityLogo from "../images/made-with-unity.svg"

export function CallToAction() {
    return <div className="cta">
        <h1 className="cta-header"><a href="https://play.unity.com/u/LuzMurilo" aria-label="UnityPlay" className="cta-link">More Games Here!</a></h1>
        <div className="cta-logo-container">
            <UnityLogo />
        </div>
    </div>
}